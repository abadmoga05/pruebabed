/*Dada una cadena de paréntesis y corchetes escribe una función que regresa si la
cadena está bien balanceada, es decir, por cada paréntesis o corchete que abre hay uno
al mismo nivel que que cierra. Por ejemplo si recibe ‘([])’ o ‘[()[]]’ tiene que regresar true y
si recibe ‘([)]’ tiene que regresar false. */
const balancedPar = function(pars){
    const stack = [];
    for (let single of pars) {
        if (single == '('){
            stack.push(single);
        }
        if(single == ')'){
            if(stack.length === 0){
                return false;
            }
            stack.pop();
        }
        
    }
    return stack.length === 0;
}
console.log(balancedPar('(['));